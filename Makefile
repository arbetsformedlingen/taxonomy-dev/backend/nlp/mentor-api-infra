#
# This Makefile handles the bootstrapping of the Aardvark infrastructure for the Mentor API
#
# Run, *=test/prod:
# make login-*
# make bootstrap-*-cluster
#

.ONESHELL:
KUBE=oc

# The project name in OpenShift
PROJECT = mentor-api

# Cluster URL's
TESTCLUSTER = "https://api.test.services.jtech.se:6443"
PRODCLUSTER = "https://api.prod.services.jtech.se:6443"

#
# Helper targets to login to OpenShift
#

.PHONY: login-test
login-test:
	@$(KUBE) login $(TESTCLUSTER) --password='' --username=''

.PHONY: login-prod
login-prod:
	@echo "\033[1;31m Warning: this will upload changes to the Prod cluster \033[0m"
	@$(KUBE) login $(PRODCLUSTER) --password='' --username=''

#
# Bootstrap helpers
#

.PHONY: gitcheck
gitcheck:
	@if [ ! -z "$$(git status --porcelain)" ]; then
		echo "**** error: repo is not clean"
		exit 1
	fi
	@if [ "$$(LANG=C git rev-parse --symbolic-full-name --abbrev-ref HEAD)" != "main" ]; then
		echo "**** error: not on the main branch"
		exit 1
	fi
	@git pull

.PHONY: check-secrets
check-secrets:
	@git submodule update --init --recursive
	@if [ ! -d "jobtech-taxonomy-api-deploy-secrets" ]; then
		echo "**** error: secrets are missing"
		exit 1
	fi

#
# Bootstrap
#

.PHONY: bootstrap
bootstrap:
	@if [ -z "$(PROJECT_ENV)" ]; then
		echo "**** error: env variable PROJECT_ENV not set"
		exit 1
	else
		oc new-project $$NAMESPACE
		# Create role-bindings to Aardvark deployer (should this point to latest? maybe a commit/tag would be better)
		$(KUBE) apply -f https://gitlab.com/arbetsformedlingen/devops/aardvark/-/raw/main/docs/aardvark-deploy-bot-access-to-project.yaml
		# Does what?
		if [ "$(PROJECT_ENV)" != "develop" ]; then
			$(KUBE) apply -f argocd/argocd-deployer-access-to-project.yaml
			cat argocd/$(PROJECT_ENV).yaml | envsubst '\$$NAMESPACE' | $(KUBE) apply -f -
		fi
		# Crete SeviceAccounts, Services, Deployments and Routes
		kustomize build kustomize/overlays/$(PROJECT_ENV) | envsubst '\$$NAMESPACE' | $(KUBE) apply -f -
		# Create Secrets
		kustomize build jobtech-taxonomy-api-deploy-secrets/mentor-api | $(KUBE) apply -f -
	fi

.PHONY: bootstrap-test-cluster
bootstrap-test-cluster: gitcheck check-secrets
	## Setup the Develop environment in the Test cluster
	@PROJECT_ENV=develop NAMESPACE=$(PROJECT)-develop \
	$(MAKE) bootstrap
	## Setup the Test environment in the Test cluster
	@PROJECT_ENV=test NAMESPACE=$(PROJECT)-test \
	$(MAKE) bootstrap

.PHONY: bootstrap-prod-cluster
bootstrap-prod-cluster: gitcheck check-secrets
	## Setup the Develop environment in the Prod cluster
	@PROJECT_ENV=prod NAMESPACE=$(PROJECT)-prod \
	$(MAKE) bootstrap
