# DevOps Infrastructure for the Mentor API
 
## Description
This repository bootstraps the DevOps infrastructure for the
[Mentor API](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/mentor-api) in OpenShift.

## Installation
These programs are needed for bootstrapping:
* [oc - the OpenShift command line](https://github.com/openshift/oc)
* [Kustomize](https://github.com/kubernetes-sigs/kustomize)
* make

## Usage
Run these steps to bootstrap the Test and Prod clusters. Start by cloning this repo, then run:
```
git submodule update --init --recursive # gets secrets
make login-test # or use another way to login
make bootstrap-test-cluster
make login-prod # or use another way to login
make bootstrap-prod-cluster
```

## Acknowledgment
Built with the [aardvark](https://gitlab.com/arbetsformedlingen/devops/aardvark) framework.

## License
See [LICENSE](./LICENSE)
